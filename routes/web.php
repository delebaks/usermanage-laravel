<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function() {
    return response()->json([
        "error" => true,
        "message" => "You shouldn't be here, but...",
        "data" => [
            "service" => "admin-api",
            "version" => env("APP_VERSION"),
        ],
    ], 200);
});

Route::group(['prefix' => 'user'], function() {
    //Route::get('adduser', 'AdminController@addUser')->middleware('jwtAuth');
    Route::post('adduser', 'AdminController@addUser');
	Route::get('deleteuser/{userid}', 'AdminController@deleteUser');
	Route::get('assigngroup/{groupid}/{userid}', 'AdminController@assignToGroup');
    Route::get('removegroup/{groupid}/{userid}', 'AdminController@removeFromGroup');
    Route::post('creategroup', 'AdminController@createGroup');
    Route::get('deletegroup/{groupid}', 'AdminController@deleteGroup');
});