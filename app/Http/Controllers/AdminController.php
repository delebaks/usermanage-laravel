<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Crypt;
//use App\Http\Controllers\User\LoadController as User;
use App\User;
use App\Groups;
use App\Group;

class AdminController extends Controller
{

    public function addUser(Request $request)
    {
        try {
            $name = $request->name;
            $email = $request->email;
            $phone = $request->phone;

            //Store data in array ready for storage
            $storeData = ['name' => $name, 'email' => $email, 'phone' => $phone];

            //Data is stored in DB
            $storeUser = User::insert($storeData);

            //if everything is good, return 200
            if($storeData)
            {   
                return response()->json(["message" => "User successfully added", "error" => false, "data" => $storeData], 200);
            }
        }catch(\Exception $e){
            return response()->json(["message" => $e->getMessage(), "error" => true, "data" => $storeData], 500);
        }

    }

    public function deleteUser(Request $request, $userid)
    {
        try{
            // This is the encryptedvalue - $encryptedvalue = Crypt::encrypt($userid);. Now let's decrypt this id so we know the userid to delete from the database. Softdelete though.
            //$rawid = Crypt::decrypt($userid);

            //For the purpose of testing, we won't encrypt the id
            $rawid = $userid;

            //update user record to deleted. Soft delete is used in this case
            $deleteUser = User::where('id', $rawid)->update(['is_deleted' => 1]);
            //if everything is good, return 200
            if($deleteUser)
            {   
                return response()->json(["message" => "User successfully deleted", "error" => false], 200);
            }
        }catch(\Exception $e){
            return response()->json(["message" => $e->getMessage(), "error" => true], 500);
        }
    }

    public function assignToGroup($groupid, $userid)
    {
        try {
            //Check if user is in group
            //For the purpose of testing, we won't encrypt Ids
            $checkUser = Groups::where([['groupid', '=', $groupid], ['userid', '=', $userid]])->first();


            if($checkUser == null)
            {
                // Add user to group if user does not belong
                $groupdata = ['groupid' => $groupid, 'userid' => $userid];
                $addToGroup = Groups::insert($groupdata);

                if($addToGroup)
                {   
                    return response()->json(["message" => "User successfully, added to group", "error" => false, "data" => $groupdata], 200);
                }
            }else{
                //User already belong to group
                return response()->json(["message" => "Sorry, user already belongs to the group", "error" => true], 500);
            }
            
        }catch(\Exception $e){
            return response()->json(["message" => $e->getMessage(), "error" => true], 500);
        }
    }

    public function removeFromGroup(Request $request, $groupid, $userid)
    {
        try {
            //Remove user from group
            $groupdata = ['group_id' => $groupid, 'userid' => $userid];
            $takeoutfromGroup = Groups::where([['groupid', '=', $groupid], ['userid', '=', $userid]])->delete();

            if($takeoutfromGroup)
            {   
                return response()->json(["message" => "User successfully deleted from group", "error" => false, "data" => $groupdata], 200);
            }
        }catch(\Exception $e){
            return response()->json(["message" => $e->getMessage(), "error" => true, "data" => $groupdata], 500);
        }
    }

    public function createGroup(Request $request)
    {
        try{
            $name = $request->name;
            $description = $request->description;

            //Create group
            $groupinfo = ['name' => $name, 'description' => $description];
            $creategroup = Group::insert($groupinfo);

            if($creategroup)
            {   
                return response()->json(["message" => "Group has been successfully created", "error" => false, "data" => $groupinfo], 200);
            }
        }catch(\Exception $e){
            return response()->json(["message" => $e->getMessage(), "error" => true], 500);
        }
    }

    public function deleteGroup(Request $request, $groupid)
    {
        try{
            //$groupid = Crypt::decrypt($groupid);

            //We will work with an encrypted id for this purpose of testing
            $groupid = $groupid;

            //Count user in group
            $checkGroup = Groups::where('groupid', $groupid)->count();

            //If members in the group equals 0, you can delete group.
            if($checkGroup == 0)
            {
                $deletegroup = Group::where('id', $groupid)->delete();

                return response()->json(["message" => "Group has been successfully deleted", "error" => false], 200);
            }else{
                return response()->json(["message" => "There are existing users in the group", "error" => true], 500);
            }
        }catch(\Exception $e){
            return response()->json(["message" => $e->getMessage(), "error" => true], 500);
        }
    }
}
